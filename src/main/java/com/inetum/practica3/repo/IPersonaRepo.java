package com.inetum.practica3.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inetum.practica3.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
